from flask import render_template
from random import shuffle
import os, codecs
from flaskext.markdown import Markdown
from flask import Flask, request, redirect, url_for
import base64
import json
import os

app = Flask(__name__)
Markdown(app)

# TOTAL NUMBER OF LINES
f = codecs.open("src/sentences-mixed-sorted.txt", "r", "utf-8")
total = len(f.readlines())
f.close()


@app.route('/', methods=['GET', 'POST'])
def index():
    # LINES LEFT TO PROCESS
    f = codecs.open("src/lines-to-process.txt", "r", "utf-8")
    lines = f.readlines()
    f.close()

    # PICK FIRST LINE OF TEXT
    line = lines[0].split("#")
    id = line[0].split("_")[1]
    sentence = line[1]

    # LEFT NUMBER OF LINES TO RECORD
    f = codecs.open("src/lines-to-process.txt", "r", "utf-8")
    leftover = len(f.readlines())
    f.close()


    # RANDOM RECORDED SENTENCE
    import glob
    waves = glob.glob("static/uploads/*.wav")
    shuffle(waves)

    if request.args.get("sent"):
        sent = True
    else:
        sent = False

    return render_template('capture.html', sentence=sentence, id=id, total=total, leftover=leftover, wave = waves[0], sent=sent)


@app.route('/saveFile', methods=['POST'])
def saveFile():

    #import ipdb
    #ipdb.set_trace()

    audio = request.form['audio'];
    line = request.form['line'];
    date = request.form['date'];

    a = open("static/uploads/sentence%s_%s.wav" % (line, date), "wb")
    #a.write(audio[22:])
    a.write(base64.b64decode(audio[22:]))
    a.close()

    # REMOVE FIRST LINE OF TEXT IF UPLOADED
    cmd = "tail -n +2 src/lines-to-process.txt > src/tmp.txt && mv src/tmp.txt src/lines-to-process.txt && rm src/tmp.txt"
    os.system(cmd)

    return json.dumps({'status':'OK','audio':audio, 'Success': True});




if __name__ == "__main__":
    app.run()

