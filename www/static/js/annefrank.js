        // CODE BASED ON
        // <https://github.com/mattdiamond/Recorderjs/blob/master/examples/example_simple_exportwav.html>
        // <http://stackoverflow.com/questions/15014638/recorderjs-uploading-recorded-blob-via-ajax>

        var audio_context;
        var recorder;
        var reader = new FileReader();
        var bla;
      
        function __log(e, data) {
            log.innerHTML += "\n" + e + " " + (data || '');
        }
      
        function startUserMedia(stream) {
            var input = audio_context.createMediaStreamSource(stream);
            __log('Media stream created.');
      
            recorder = new Recorder(input);
            __log('Recorder initialised.');
        }

        function startRecording(button) {
            recorder && recorder.record();
            button.disabled = true;
            button.nextElementSibling.disabled = false;
            __log('Recording...');
        }

        function stopRecording(button) {
            recorder && recorder.stop();
            button.disabled = true;
            button.previousElementSibling.disabled = false;
            __log('Stopped recording.');
            
            // create WAV download link using audio data blob
            createDownloadLink();
            recorder.clear();
        }

        function createDownloadLink() {
            recorder && recorder.exportWAV(function(blob) {
                var url = URL.createObjectURL(blob);
                var li = document.createElement('li');
                var au = document.createElement('audio');
                var hf = document.createElement('a');
                bla = blob;
                
                au.controls = true;
                au.src = url;
                hf.href = url;
                hf.download = "Download: " + $("#sentence").attr("data-id") + "_" + new Date().toDateString() + "_" + new Date().toLocaleTimeString() + '.wav';
                hf.innerHTML = hf.download;
                li.appendChild(hf);
                li.appendChild(au);
                //recordingslist.replaceChild(li);
                $("#recordingslist").html(li);
                document.getElementById("send").disabled = false;


            // this is triggered once the blob is read and readAsDataURL returns
            reader.onload = function (event) {
                var formData = new FormData();
                formData.append('date', new Date().toDateString() + "_" + new Date().toLocaleTimeString() );
                formData.append('line', $("#sentence").attr("data-id"));
                formData.append('audio', event.target.result);

                // CHECKS WHAT'S INSIDE formData
                //for (var pair of formData.entries()) {
                //    console.log(pair[0]+ ', ' + pair[1]); 
                //}

                $.ajax({
                    type: 'POST',
                    url: '/saveFile',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    cache: false,
                    success: function (json) {
                        if (json.Success) {
                            // do successful audio upload stuff
                            location.href += "?sent=true";
                            document.getElementById("success").style.display = "block";
                        } else {
                            // handle audio upload failure reported back from server (I have a json.Error.Msg)
                        }
                    }
                    , error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error! '+ textStatus + ' - ' + errorThrown + '\n\n' + jqXHR.responseText);
                        // handle audio upload failure
                    }
                });
            }
        });
    }

    window.onload = function init() {
        // CHECKS BROWSER COMPATIBILITY
        try {
            // webkit shim
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            window.URL = window.URL || window.webkitURL;
            
            audio_context = new AudioContext;
            __log('Audio context set up.');
            __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
        } catch (e) {
            window.alert('No web audio support in this browser!');
        }
        
        navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
            __log('No live audio input: ' + e);
            window.alert("If you want to record a sentence, please use Chrome or Firefox >53.0.3");
        });



    };

$(document).ready(function(){
    // READ MORE
    $(".read-more").click(function(){
       $(this).parent().next().show(); 
    });
    
    $(".about .close").click(function(){
        $(this).parent().hide();
    });
    $("#success .close").click(function(){
        $(this).parent().hide();
    });

    // LANGUAGE CHOOSER
    $("nav button").click(function(){
        lang = $(this).attr("id");
        $("div[lang=nl], h2[lang=nl]").hide();
        $("div[lang=en], h2[lang=en]").hide();
        $("div[lang=fr], h2[lang=fr]").hide();
        $("div[lang="+lang+"], h2[lang="+lang+"]").show();
        $("nav button").addClass("inactive");
        $(this).removeClass("inactive");
    });
});

